import React, { Component } from "react"
import "./style.css"

class Item extends Component {
    render() {
        const { data } = this.props
        return <div className='red' style={{ fontSize: '20px' }}>{data}</div>
    }
}

export default Item