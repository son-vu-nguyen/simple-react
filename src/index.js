import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import './index.css';

import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import configureStore from "./redux/store";
import createHistory from "history/createBrowserHistory";

const history = createHistory();
const store = configureStore(history);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
