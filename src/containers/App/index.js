import React, { Component } from 'react';
import { connect } from "react-redux";
import './style.css';

import Item from "../../components/Item"

import { changeInputData } from "./action"
import { getItemList } from "../../redux/itemList/action"
import { inputDataSelector } from "./selector"
import { itemListDataSelector, itemListErrorSelector, itemListLoadingSelector } from "../../redux/itemList/selector"

class App extends Component {
  componentDidMount() {
    this.props.boundGetItemList()
  }

  render() {
    const data_list = [
      '123',
      '234',
      '345',
      '456',
      '567'
    ]
    const { input_data, boundChangeInputData, itemListData, itemListLoading, itemListError } = this.props
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div>
          {data_list.map((x, index) =>
            <Item key={index} data={x} />
          )}
        </div>

        <div>
          <input type="text" value={input_data} onChange={boundChangeInputData} />
          <div>
            Your input here: {input_data}
          </div>
        </div>

        <div>
          <h1>Item list from server:</h1>
          <div>Loading: {itemListLoading.status}</div>
          <div>Error: {itemListError}</div>
          <div>Data:</div>
          {itemListData && <div>
            {itemListData.map((item, index) => <Item key={index} data={item} />)}
          </div>}

        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    input_data: inputDataSelector(state),
    itemListData: itemListDataSelector(state),
    itemListLoading: itemListLoadingSelector(state),
    itemListError: itemListErrorSelector(state)
  };
};
const mapDispatchToProps = dispatch => ({
  boundChangeInputData: e => dispatch(changeInputData(e.target.value)),
  boundGetItemList: () => dispatch(getItemList())
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
