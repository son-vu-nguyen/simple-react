import { combineReducers } from "redux";
import { INPUT_CHANGE } from "./constant";

export function inputDataReducer(state = '', action) {
    switch (action.type) {
        case INPUT_CHANGE: {
            const { data } = action;
            return data;
        }
        default:
            return state;
    }
}

export default combineReducers({
    input_data: inputDataReducer
});  