const appPageSelector = state => state.app_page;

export const inputDataSelector = state => appPageSelector(state).input_data