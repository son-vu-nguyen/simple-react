import { INPUT_CHANGE } from "./constant"

export function changeInputData(data) {
    return dispatch => {
        dispatch({
            type: INPUT_CHANGE,
            data: data
        })
    }
}