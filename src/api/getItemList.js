// this is just fake request, define your request here
const sleep = () => new Promise(resolve => setTimeout(resolve, 3000))
const makeRequest = async () => {
    await sleep()
    return ['Item 1', 'Item 2', 'Item 3']
}

export default async function () {
    return await makeRequest()
}
