import { GET_ITEM_LIST } from "./constant";
import getItemListApi from "../../api/getItemList";

export function getItemList() {
    return async dispatch => {
        dispatch({ type: GET_ITEM_LIST.requested() });
        try {
            const result = await getItemListApi();
            dispatch({ type: GET_ITEM_LIST.succeeded(), itemList: result });
        } catch (error) {
            const message = error.response.data.error;
            dispatch({ type: GET_ITEM_LIST.failed(), error: message });
        }
    };
}