import { combineReducers } from "redux";
import LoadingStatus from "../../utils/LoadingStatus";
import { GET_ITEM_LIST } from "./constant";

function itemListReducer(state = null, action) {
    switch (action.type) {
        case GET_ITEM_LIST.succeeded(): {
            return action.itemList;
        }
        default:
            return state;
    }
}

function loadingReducer(state = new LoadingStatus(), action) {
    switch (action.type) {
        case GET_ITEM_LIST.requested():
            return state.setLoading();

        case GET_ITEM_LIST.succeeded():
        case GET_ITEM_LIST.failed():
            return state.setLoaded();
        default:
            return state;
    }
}

function errorReducer(state = null, action) {
    switch (action.type) {
        case GET_ITEM_LIST.failed():
            return action.error;

        case GET_ITEM_LIST.succeeded():
            return null;

        default:
            return state;
    }
}

export default combineReducers({
    data: itemListReducer,
    loading: loadingReducer,
    error: errorReducer
});
