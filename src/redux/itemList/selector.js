const itemListSelector = (state) => state.itemList

export const itemListDataSelector = (state) => itemListSelector(state).data

export const itemListLoadingSelector = (state) => itemListSelector(state).loading

export const itemListErrorSelector = (state) => itemListSelector(state).error