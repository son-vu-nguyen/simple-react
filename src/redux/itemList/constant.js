import AsyncActionType from "../../utils/AsyncActionType";

export const GET_ITEM_LIST = new AsyncActionType("GET_ITEM_LIST");