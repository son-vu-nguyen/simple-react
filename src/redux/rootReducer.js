import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

import AppPageReducer from "../containers/App/reducer"
import ItemListReducer from "./itemList/reducer"

export default combineReducers({
    router: routerReducer,
    app_page: AppPageReducer,
    itemList: ItemListReducer
});
