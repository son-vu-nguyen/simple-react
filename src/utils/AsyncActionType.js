export default class AsyncActionType {
  constructor(base) {
    this.base = base;
  }

  requested() {
    return `${this.base}:REQUESTED`;
  }

  succeeded() {
    return `${this.base}:SUCCEEDED`;
  }

  failed() {
    return `${this.base}:FAILED`;
  }

  canceled() {
    return `${this.base}:CANCELED`;
  }
}
