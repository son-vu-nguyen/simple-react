const LOADING_STATUS = "LOADING";
const NOT_LOADED_STATUS = "NOT_LOADED";
const LOADED_STATUS = "LOADED";

export default class LoadingStatus {
  static Loading() {
    return new LoadingStatus(LOADING_STATUS);
  }

  static Loaded() {
    return new LoadingStatus(LOADED_STATUS);
  }

  constructor(state = NOT_LOADED_STATUS) {
    this.status = state;
  }

  setLoading() {
    return new LoadingStatus(LOADING_STATUS);
  }

  setLoaded() {
    return new LoadingStatus(LOADED_STATUS);
  }

  isNotLoaded() {
    return this.status === NOT_LOADED_STATUS;
  }

  isLoading() {
    return this.status === LOADING_STATUS;
  }

  isLoaded() {
    return this.status === LOADED_STATUS;
  }
}
